﻿using System;
using System.Threading;
using Acr.UserDialogs;
using Acr.UserDialogs.Builders;
using Android.App;

namespace DataFunc.Extensions.Xamarin.Android
{
    public static class UserDialogsInstanceExtensions
    {
        public static void ShowNoInternetAvailable(this IUserDialogs dialogsService, int closeAfterAmountOfSeconds = 5)
        {
            Thread.Sleep(TimeSpan.FromMilliseconds(100));
            dialogsService.Toast("Er is geen actieve internetverbinding, probeer het later opnieuw", TimeSpan.FromSeconds(closeAfterAmountOfSeconds));
        }

        public static void ShowGenericError(this IUserDialogs dialogsService, int closeAfterAmountOfSeconds = 5)
        {
            Thread.Sleep(TimeSpan.FromMilliseconds(100));
            dialogsService.Toast("Er een probleem opgetreden, probeer het later opnieuw", TimeSpan.FromSeconds(closeAfterAmountOfSeconds));
        }

        public static void ShowAlert(this AlertBuilder builder, Activity activity, string title, string message, Action callBackAction)
        {
            builder.Build(activity, new AlertConfig
            {
                Message = message,
                Title = title,
                OkText = "Ok",
                OnAction = callBackAction
            }).Show();
        }
    }
}