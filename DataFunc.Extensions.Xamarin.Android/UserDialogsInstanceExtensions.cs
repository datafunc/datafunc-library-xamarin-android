﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using Acr.UserDialogs;
using Acr.UserDialogs.Builders;
using Android.App;
using Android.Content;
using Android.Widget;

namespace DataFunc.Extensions.Xamarin.Android
{
    public static class UserDialogsInstanceExtensions
    {
        public static void ShowNoInternetAvailable(this IUserDialogs dialogsService, int closeAfterAmountOfSeconds = 7)
        {
            Thread.Sleep(TimeSpan.FromMilliseconds(200));
            dialogsService.Toast("Er is geen actieve internetverbinding, probeer het later opnieuw", TimeSpan.FromSeconds(closeAfterAmountOfSeconds));
        }

        public static void ShowGenericError(this IUserDialogs dialogsService, int closeAfterAmountOfSeconds = 7)
        {
            dialogsService.Toast("Er een probleem opgetreden, probeer het later opnieuw", TimeSpan.FromSeconds(closeAfterAmountOfSeconds));
        }

        public static Toast ShowShortToast(Context context, string text)
        {
            return Toast.MakeText(context, text, ToastLength.Short);
        }
        public static void ShowAlert(this AlertBuilder builder, Activity activity, string title, string message, Action callBackAction = null, IDialogInterfaceOnDismissListener dismissListener = null)
        {
            var alertConfig = new AlertConfig
            {
                Message = message,
                Title = title,
                OkText = "Ok",
            };

            if (callBackAction != null)
                alertConfig.OnAction = callBackAction;

            var buildedAlert = builder.Build(activity, alertConfig);
            
            if (dismissListener != null)
                buildedAlert.SetOnDismissListener(dismissListener);
    
            buildedAlert.Show();
        }
    }
}