﻿using System;
using System.Threading;
using Android.OS;

namespace DataFunc.Extensions.Xamarin.Android.BackgroundTasks.Infrastructure
{
    public class BackgroundTaskBuilder<T, TY> where T : BackgroundTask<TY>, new()
    {
        private static T _toExecuteTask;
        private Action<BackgroundTaskResult> _onBeforeCallBack;

        private Action<BackgroundTaskResult> _successCallBack;

        private Action<BackgroundTaskResult> _errorCallBack;

        private Action<BackgroundTaskResult> _completeCallBack;

        private Action<BackgroundTaskResult> _noInternetAction;

        private TY[] _parameters = new TY[0];

        private readonly CancellationToken _token = CancellationToken.None;

        private bool _showLoadingIndicator = false;

        private BackgroundTaskBuilder()
        {
        }

        public static BackgroundTaskBuilder<T, TY> FromTask()
        {
            return new BackgroundTaskBuilder<T, TY>();
        }

        public BackgroundTaskBuilder<T, TY> WithLoadingIndicator()
        {
            _showLoadingIndicator = true;
            return this;
        }

        public BackgroundTaskBuilder<T, TY> WithParameters(params TY[] parameters)
        {
            _parameters = parameters;
            return this;
        }
        public BackgroundTaskBuilder<T, TY> WithBeforeCallBack(Action<BackgroundTaskResult> callBackAction)
        {
            _onBeforeCallBack = callBackAction;
            return this;
        }

        public BackgroundTaskBuilder<T, TY> WithCompleteCallBack(Action<BackgroundTaskResult> callBackAction)
        {
            _completeCallBack = callBackAction;
            return this;
        }

        public BackgroundTaskBuilder<T, TY> WithSuccessAction(Action<BackgroundTaskResult> callBackAction)
        {
            _successCallBack = callBackAction;
            return this;
        }
        public BackgroundTaskBuilder<T, TY> WithErrorAction(Action<BackgroundTaskResult> callBackAction)
        {
            _errorCallBack = callBackAction;
            return this;
        }
        public BackgroundTaskBuilder<T, TY> WithNoInternetAction(Action<BackgroundTaskResult> callBackAction)
        {
            _noInternetAction = callBackAction;
            return this;
        }

        public BackgroundTaskBuilder<T, TY> WithNoInternetThenGotoError()
        {
            _noInternetAction = _errorCallBack;
            return this;
        }

        public BackgroundTaskBuilder<T, TY> WithNoInternetThenGotoComplete()
        {
            _noInternetAction = _completeCallBack;
            return this;
        }

        public AsyncTask<TY, int, BackgroundTaskResult> Execute()
        {
            _toExecuteTask = new T();
            _toExecuteTask.Prepare(_parameters, _successCallBack, _errorCallBack, _completeCallBack, _noInternetAction, _showLoadingIndicator,_onBeforeCallBack, _token);
            return _toExecuteTask.Execute(_parameters);
        }


    }
}