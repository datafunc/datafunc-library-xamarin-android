﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Telephony;
using DataFunc.Exceptions;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Org.Apache.Http;
using Object = Java.Lang.Object;

namespace DataFunc.Extensions.Xamarin.Android.BackgroundTasks.Infrastructure
{
    public abstract class BackgroundTask<T> : AsyncTask<T, int, BackgroundTaskResult>
    {
        protected Dictionary<string, string> LoggingProperties { get; set; }
        protected abstract bool RequiresOnlineActivity { get; }
        protected T[] Parameters;
        private Action<BackgroundTaskResult> _successCallBack;
        private Action<BackgroundTaskResult> _errorCallBack;
        private Action<BackgroundTaskResult> _onCompleteCallBack;
        private Action<BackgroundTaskResult> _noInternetCallBack;
        private Action<BackgroundTaskResult> _onBeforeCallBack;
        private CancellationToken _token;
        private bool _showLoadingIndicator;


        public void Prepare(T[] parameters, Action<BackgroundTaskResult> successCallBack,
            Action<BackgroundTaskResult> errorCallBack, Action<BackgroundTaskResult> onCompleteCallBack,
            Action<BackgroundTaskResult> noInternetCallBack, bool showLoadingIndicator,
            Action<BackgroundTaskResult> onBeforeCallBack, CancellationToken token)
        {
            Parameters = parameters;
            _onBeforeCallBack = onBeforeCallBack;
            _successCallBack = successCallBack;
            _errorCallBack = errorCallBack;
            _onCompleteCallBack = onCompleteCallBack;
            _noInternetCallBack = noInternetCallBack;
            _showLoadingIndicator = showLoadingIndicator;
            _token = token;

        }


        private void BuildLoggingProperties()
        {
            LoggingProperties =
            new Dictionary<string, string>
               {
                   {
                       nameof(BackgroundTask<T>), this.GetType().Name
                   },
                   {
                       nameof(Parameters), JsonConvert.SerializeObject(Parameters)
                   },
                   {
                       nameof(InternetAvailable), InternetAvailable.ToString()
                   }
               };
        }
        protected override void OnPreExecute()
        {
            BuildLoggingProperties();

            Analytics.TrackEvent(nameof(OnPreExecute), LoggingProperties);

            base.OnPreExecute();

            if (!InternetAvailable && RequiresOnlineActivity)
                _showLoadingIndicator = false; // disables flickering

            if (_showLoadingIndicator)
                UserDialogs.Instance.ShowLoading("Even geduld", MaskType.Gradient);

            _onBeforeCallBack?.Invoke(null);
        }

        protected abstract Task<object> Process(CancellationToken token);

        // ReSharper disable once RedundantOverriddenMember
        protected override void OnPostExecute(Object result)
        {
            base.OnPostExecute(result);
        }

        // ReSharper disable once RedundantOverriddenMember
        protected override void OnPostExecute(BackgroundTaskResult result)
        {
            Analytics.TrackEvent(nameof(OnPostExecute), LoggingProperties);

            if (_showLoadingIndicator)
                UserDialogs.Instance.HideLoading();

            if (result.Success)
                _successCallBack?.Invoke(result);
            else
            {
                if (!result.IsOnline)
                    _noInternetCallBack?.Invoke(result);
                else if (!result.Success)
                    _errorCallBack?.Invoke(result);
            }



            _onCompleteCallBack?.Invoke(result);
        }

        protected override BackgroundTaskResult RunInBackground(params T[] @params)
        {
            var result = new BackgroundTaskResult();

            if (!InternetAvailable && RequiresOnlineActivity)
            {
                result.IsOnline = false;
                result.Success = false;
            }
            else
            {
                try
                {
                    result.IsOnline = InternetAvailable;
                    result.Result = Process(_token).GetAwaiter().GetResult();
                    result.Success = true;

                }
                catch (HttpRequestException ex) when (!string.IsNullOrWhiteSpace(ex.Message) && ex.Message.Contains("no such host is known", StringComparison.CurrentCultureIgnoreCase))
                {
                    result.Success = false;
                    result.IsOnline = false;
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, LoggingProperties);
                    result.Success = false;
                    result.Exception = ex;
                }
            }

            return result;
        }

        public bool InternetAvailable
        {
            get
            {
                var connectivityManager =
                    (ConnectivityManager)global::Android.App.Application.Context.GetSystemService(
                        Context.ConnectivityService);
                return connectivityManager.ActiveNetworkInfo != null &&
                       connectivityManager.ActiveNetworkInfo.IsConnected;
            }
        }




    }

}