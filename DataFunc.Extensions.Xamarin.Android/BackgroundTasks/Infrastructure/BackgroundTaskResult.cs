﻿using System;
using System.Runtime.Serialization;

namespace DataFunc.Extensions.Xamarin.Android.BackgroundTasks.Infrastructure
{
    public class BackgroundTaskResult
    {
        public object Result { get; set; }
        public bool IsOnline { get; set; }
        public bool Success { get; set; }
        public Exception Exception { get; set; }
    }
}