﻿using Android.Content;
using Newtonsoft.Json;

namespace DataFunc.Extensions.Xamarin.Android
{
    public static class SharedPreferencesExtensions
    {
      
        public static void PutJson<T>(this ISharedPreferencesEditor preferences,string key, T objectToStore)
        {
            preferences.PutString(key, JsonConvert.SerializeObject(objectToStore));
        }

        public static T GetJson<T>(this ISharedPreferences preferences, string key)
        {
            if (!preferences.Contains(key))
                return default;

            var jsonString = preferences.GetString(key, string.Empty);
            if (string.IsNullOrWhiteSpace(jsonString))
                return default;

            return JsonConvert.DeserializeObject<T>(jsonString);
        }
    }
}